// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package wd

import (
	"reflect"
	"testing"
)

func TestDiff(t *testing.T) {
	type testCase struct {
		a, b string
		ws   bool
		want []component
	}
	testCases := []testCase{
		{"xxx", "xxx", false, []component{{value: "xxx"}}},
		{"xxx", "", false, []component{{value: "xxx", status: diffRemoved}}},
		{"", "xxx", false, []component{{value: "xxx", status: diffAdded}}},
		{"aaa bbb", "aaa    bbb", false, []component{{value: "aaa"}, {value: "    ", status: diffAdded}, {value: " ", status: diffRemoved}, {value: "bbb"}}},
		{"aaa bbb", "aaa    bbb", true, []component{{value: "aaa    bbb"}}},
	}
	for _, tc := range testCases {
		cmp := comparer{tc.ws}
		got := cmp.diff(tc.a, tc.b)
		if !reflect.DeepEqual(got, tc.want) {
			t.Errorf("want %#v, got %#v", tc.want, got)
		}
	}
}
