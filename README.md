wd [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/wd?status.svg)](http://godoc.org/gitlab.com/opennota/wd) [![Pipeline status](https://gitlab.com/opennota/wd/badges/master/pipeline.svg)](https://gitlab.com/opennota/wd/commits/master)
==

A package for comparing strings on a word per word basis and generating a coloured diff.

## Install

    go get -u gitlab.com/opennota/wd

